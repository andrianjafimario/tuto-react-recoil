import logo from "./logo.svg";
import "./App.css";
import QuoteCalculation from "./components/QuoteCalculation";
import { RecoilRoot } from "recoil";

function App() {
  return (
    <div className="App">
      <RecoilRoot>
        
        <QuoteCalculation />
      </RecoilRoot>
    </div>
  );
}

export default App;
