import { atom } from "recoil";

export const UnitePriceState = atom({
  key: 'UnitePriceState',
  default: '',
});

export const quantityState = atom({
  key: 'quantityState',
  default: '',
});

export const withTaxState = atom({
  key: 'withTaxState',
  default: false,
});
