import { Container } from "react-bootstrap"

const Header = ()=>{
  return (<Container fluid>
    <header>
      <h1>Application de calcul de devis</h1>
    </header>
  </Container>)
}

export default Header;

