import { Button, Col, Container, Form, Row } from "react-bootstrap";
import { useRecoilState } from "recoil";
import {
  quantityState,
  UnitePriceState,
  withTaxState,
} from "../States/QuoteCalculationState";
import InputAnimatedLabel from "./input-animated-label/InputAnimatedLabel";
import React from "react";
import Pdf from "react-to-pdf";

const ref = React.createRef();
const taxPercentage = 5;

const calculateWithTaxe = (number) => {
  return (number * taxPercentage) / 100 + number;
};

const EntryForm = () => {
  const [unitePrice, setUnitePrice] = useRecoilState(UnitePriceState);
  const [quantity, setQuantity] = useRecoilState(quantityState);
  const [withTax, setTaxStatus] = useRecoilState(withTaxState);

  const handleChangeUnitPrice = (value) => {
    setUnitePrice(value);
    Calculate();
  };
  const handleChangeQuantity = (value) => {
    setQuantity(value);
    Calculate();
  };
  const handleChangeTaxStatus = (e) => {
    setTaxStatus(e.target.checked);
  };

  const Calculate = () => {
    let number = parseInt(unitePrice) * parseInt(quantity) * (4, 425.1672); //euro to ar
    return withTax ? calculateWithTaxe(number) : number.toFixed(2);
  };

  return (
    <Container fluid>
      <Form ref={ref}>
        <Row>
          <Col>
            <InputAnimatedLabel
              id="UnitePrice"
              label="Prix Unitaire  (En Euro)"
              name="UnitePrice"
              type="number"
              value={unitePrice}
              handleChange={handleChangeUnitPrice}
            />
          </Col>
          {/* <Col>
        <select >
    
        </select>
        </Col> */}
          <Col>
            <InputAnimatedLabel
              id="quantity"
              label="Quantité"
              name="quantity"
              type="number"
              value={quantity}
              handleChange={handleChangeQuantity}
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <Form.Group>
              <input
                className="borderdOnTop"
                type="checkbox"
                id="withTaxe"
                checked={withTax}
                onChange={handleChangeTaxStatus}
              />
              <label htmlFor="withTaxe">Avec taxe?</label>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col>
            <h5 className="borderdOnTop">Total:</h5>
          </Col>
          <Col>
            {" "}
            <h5 className="borderdOnTop">
              {isNaN(Calculate()) ? "0" : Calculate()} (en Ariary){" "}
            </h5>{" "}
          </Col>
        </Row>
      </Form>
      <div className="borderdOnTop"></div>
      <div className="borderdOnTop container">
        <Pdf
          targetRef={ref}
          filename="calcul de devis.pdf"
        >
          {({ toPdf }) => <Button onClick={toPdf} variant="primary">Imprimer</Button>}
          
        </Pdf>
      </div>
    </Container>
  );
};

export default EntryForm;
