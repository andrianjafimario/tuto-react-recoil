import React, { useState } from 'react';
import classNames from 'classnames';
//@ts-ignore
import styles from './InputAnimatedLabel.module.css';

const InputAnimatedLabel= ({
  id,
  name,
  label,
  type,
  value,
  handleChange,
  width = '',
}) => {
  const [emptyValue, setValueEmptyStatus] = useState(true);

  return (
    <div
      className={styles.AnimatedformGroup}
      style={{
        width: width,
      }}
    >
      <label
        className={classNames(
          styles.animatedLBL,
          emptyValue && !value ? styles.empty : styles.emptyFocus
        )}
        htmlFor={id}
      >
        {label}:
      </label>
      <input
        className={classNames('noBorder', styles.Input)}
        type={type}
        name={name}
        id={id}
        value={value}
        onChange={(e) => {
          handleChange(e.target.value);
        }}
        onFocus={() => {
          setValueEmptyStatus(false);
        }}
        onBlur={() => {
          setValueEmptyStatus(true);
        }}
      />
    </div>
  );
};

export default InputAnimatedLabel;
