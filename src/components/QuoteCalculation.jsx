import { Container } from "react-bootstrap";
import EntryForm from "./EntryForm";
import Header from "./Header";

const QuoteCalculation = () => {
  return (
    <div>
      <Container>
        <Header />
        <EntryForm />
      </Container>
    </div>
  );
};

export default QuoteCalculation;
